package cmpe434lab7;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.CommonLCD;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.NXTColorSensor;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.robotics.LightDetectorAdaptor;
import lejos.robotics.SampleProvider;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

public class Subsumption {
	static EV3ColorSensor lightSensorEV3 = new EV3ColorSensor(SensorPort.S4);
	static LightDetectorAdaptor lightAdaptorEV3 = new LightDetectorAdaptor((SampleProvider) lightSensorEV3);
	static NXTLightSensor lightSensorOld = new NXTLightSensor(SensorPort.S1);
	static LightDetectorAdaptor lightAdaptorOld = new LightDetectorAdaptor((SampleProvider) lightSensorOld);

	static EV3LargeRegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
	static EV3LargeRegulatedMotor rightMotor = new EV3LargeRegulatedMotor(MotorPort.D);
	static float m = 0.8430854072100f;
	static float b = 0.3267436552749f;

	static  boolean rotateSuppres;
	static  boolean followSuppres;
	static final float TRESHOLD = 0.45f;
	static final int SPEED_MULT = 500;
	
	public static void main(String[] args) {
		Behavior follow=new Behavior(){
			@Override
			public boolean takeControl() {
				return (lightAdaptorOld.getLightValue() > TRESHOLD);
			}

			@Override
			public void action() {
				float valueEv3 = lightAdaptorEV3.getLightValue();
				float valueOld = lightAdaptorOld.getLightValue();
				float speedEv3 = getCalibratedValue(valueEv3) * SPEED_MULT;
				float speedOld = valueOld * SPEED_MULT;
				leftMotor.setSpeed(speedEv3);
				rightMotor.setSpeed(speedOld);
				leftMotor.forward();
				rightMotor.forward();
				Thread.yield();
			}

			@Override
			public void suppress() {
				
			}
		};
		
		Behavior rotate=new Behavior() {
			boolean suppressed=false;
			@Override
			public boolean takeControl() {
				return true;
			}

			@Override
			public void action() {
				if(suppressed){
					suppressed=false;
					return;
				}
				leftMotor.setSpeed(SPEED_MULT/4);
				rightMotor.setSpeed(SPEED_MULT/4);
				leftMotor.backward();
				rightMotor.forward();
				Thread.yield();
			}

			@Override
			public void suppress() {
				suppressed=true;
			}
		};
		Behavior[] behaviors={rotate,follow};
		(new Arbitrator(behaviors)).go();
	}
	
	
	public static float getCalibratedValue(float value) {
		return m * value + b;
	}


}
